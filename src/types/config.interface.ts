import { IHost } from './host.interface';

export interface IConfig {
    windows_os?: boolean;
    watch_interval: number;
    hosts: IHost[];
}
