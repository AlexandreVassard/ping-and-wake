import { IHost } from '../types/host.interface';
import { Config } from './config';
import { Logger } from './logger';
import { Pinger } from './pinger';
import { Waker } from './waker';

export class Checker {
    private windows_os: boolean;
    private hosts: IHost[];
    private interval: number;
    private checking: boolean;

    constructor(config: Config) {
        const variables = config.get();
        this.windows_os = !!variables.windows_os;
        this.checking = false;
        this.interval = variables.watch_interval * 1000;
        this.hosts = variables.hosts;
        Logger.log(
            `Checking ${variables.watch_interval} seconds interval for devices to wake`
        );
        this.check();
        setInterval(this.check.bind(this), this.interval);
    }

    async check() {
        if (!this.checking) {
            this.checking = true;
            for (const host of this.hosts) {
                const hostAlive = await Pinger.isAlive(host.ip);
                if (!hostAlive) {
                    Logger.log(`${host.ip} - Isn't alive, sending WOL order`);
                    const waked = await Waker.wake(host, this.windows_os);
                    if (waked) {
                        Logger.log(`${host.ip} - WOL order sent !`);
                    } else {
                        Logger.log(`${host.ip} - Couldn't send WOL order !`);
                    }
                }
            }
            this.checking = false;
        }
    }
}
