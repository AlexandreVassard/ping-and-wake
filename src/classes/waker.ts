import { WakeOptions, wake } from 'wol';
import { IHost } from '../types/host.interface';

export class Waker {
    public static wake(host: IHost, windows_os: boolean): Promise<boolean> {
        const options: WakeOptions = {
            address: host.ip,
        };
        const wakeOptions = windows_os ? options : undefined;
        return wake(host.mac, wakeOptions);
    }
}
