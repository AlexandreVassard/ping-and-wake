# Ping and Wake

This is a simple application to always Wake On Lan devices that are offline.

They are detected by a ping command.

If ping isn't responding, we wake the device.

## Configuration file

It's really simple to use it.

Just provide all devices that you need to wake like in `config.example.json`.

If you are running this script on Windows, please set `windows_os: true`.

Otherwise, you won't wake anything on Windows.

## Usage

Clone this git repository :

```
git clone https://gitlab.com/AlexandreVassard/ping-and-wake.git
```

Create a `config.json` file :

```
cd ping-and-wake
cp config.example.json config.json
```

You need to set what you need to wake in this file.

### Docker

Run the docker container if you have `docker-compose` :

```
docker-compose up -d
```

Or :

```
docker compose up -d
```

### NodeJS

Install dependencies and build the project :

```
npm install && npm run build
```

Run the script :

```
npm run start
```
