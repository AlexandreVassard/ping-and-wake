export interface IHost {
    ip: string;
    mac: string;
}
