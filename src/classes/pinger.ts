import ping from 'ping';

export class Pinger {
    public static ping(host: string) {
        return ping.promise.probe(host);
    }

    public static async isAlive(host: string): Promise<boolean> {
        const pingResult = await Pinger.ping(host);
        return pingResult.alive;
    }
}
