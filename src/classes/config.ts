import { IConfig } from '../types/config.interface';
import * as fs from 'fs';
import { join } from 'path';
import { Logger } from './logger';

export class Config {
    public variables: IConfig;

    constructor() {
        const configFilePath = join(__dirname, '../../config.json');
        if (!fs.existsSync(configFilePath)) {
            throw new Error('You need to provide a "config.json" file.');
        }
        const configFile = fs.readFileSync(configFilePath);
        this.variables = JSON.parse(configFile.toString()) as IConfig;
        this.checkVariables();
    }

    public get(): IConfig {
        return this.variables;
    }

    private checkVariables() {
        if (
            !this.variables.watch_interval ||
            isNaN(this.variables.watch_interval)
        ) {
            throw new Error(
                'You need to provide a "watch_interval" variable of type "number" in "config.json"'
            );
        }
        if (this.variables.windows_os === undefined) {
            Logger.log(
                'WARNING! If you are running this script on windows, please provide "windows_os" variable to true in "config.json"'
            );
        }
        if (!this.variables.hosts) {
            throw new Error(
                'You need to provide a "hosts" variable in "config.json"'
            );
        }
        for (const host of this.variables.hosts) {
            if (!host.ip || !host.mac) {
                throw new Error(
                    'You need to provide an "ip" and a "mac" for each host in "config.json"'
                );
            }
            if (!/^([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})$/.test(host.mac)) {
                throw new Error(`${host.mac} is not a correct mac address !`);
            }
        }
    }
}
