import { Checker } from './classes/checker';
import { Config } from './classes/config';

async function bootstrap() {
    const config = new Config();
    new Checker(config);
}
bootstrap();
