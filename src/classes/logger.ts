import moment from 'moment';

export class Logger {
    public static log(message: string): void {
        const date = moment(Date.now()).format('MM/DD/YYYY HH:mm:ss');
        console.log(`[${date}] - ${message}`);
    }
}
